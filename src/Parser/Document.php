<?php
/**
 * Project  Frostmourne HP (3.3.5/4.3.4 Full Support)
 *
 *  @link         http://www.frostmourne.eu/
 *  @copyright    Copyright (c) 2009 - 2016 Frostmourne
 *  @version      v4.0.1a
 */

/**
 * Created by PhpStorm.
 * User: bitcoding
 * Date: 13.06.16
 * Time: 21:29
 */

namespace Parser\Parser;


/**
 * Class Document
 * @package Parser\Parser
 * 
 * @param Node[] childNodes
 * @param array attributes
 * @param string name
 * @param string value
 */
class Document extends Node
{
    /**
     * @var string
     */
    public $type = self::TYPE_DOCUMENT;
    
    function createNewText($text){
        return new Text($text);
    }
    function createNewElement($name){
        return new Element($name);
    }
}