<?php
/**
 * Project  Frostmourne HP (3.3.5/4.3.4 Full Support)
 *
 *  @link         http://www.frostmourne.eu/
 *  @copyright    Copyright (c) 2009 - 2016 Frostmourne
 *  @version      v4.0.1a
 */

namespace Parser\Parser\Engine;

use Parser\ParserEngine;
use Parser\Parser\Document;
use Parser\Parser\Node;

class WikiEngine extends ParserEngine
{
    function parse($text){}
    function render(Node $node,&$list = null ,$disable = false){}
}