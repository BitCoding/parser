<?php
/**
 * Project  Frostmourne HP (3.3.5/4.3.4 Full Support)
 *
 *  @link         http://www.frostmourne.eu/
 *  @copyright    Copyright (c) 2009 - 2016 Frostmourne
 *  @version      v4.0.1a
 */

namespace Parser\Parser;


/**
 * Class Text
 * @package Parser\Parser
 */
class Text extends Node
{
    /**
     * @var string
     */
    public $type = self::TYPE_TEXT;
}